import { Component, OnInit } from '@angular/core';
import { IUser } from 'src/app/interfaces/user.interface';
import { UsersService } from 'src/app/services/users/users.service';

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})


export class TeamComponent implements OnInit {

  users: IUser[] = []
  usersFilter: IUser[] = [];
  searchTerms = '';
  constructor(
    private usersService: UsersService
  ) { }


  ngOnInit(): void {
    this.users = this.usersService.getAllUsers();
    this.usersFilter = this.users;
  }

  filterUsers() {
    this.usersFilter = this.users.filter( user => {
      if (
        user.name.includes(this.searchTerms) ||
        user.lastName.includes(this.searchTerms) ||
        user.position.includes(this.searchTerms) ||
        user.department.includes(this.searchTerms) ||
        user.email.includes(this.searchTerms)
      ) {
        return user;
      } else {
        return;
      }
    });
  }

}
