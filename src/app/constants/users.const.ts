export const USERS = [
    {
        name: "Jane",
        lastName: "Cooper",
        position: "Regional Paradigm Technician",
        department: "Optimization",
        urlImage: "assets/img/contacts/jane_cooper.png",
        stateWork: 0,
        pinned: true,
        email: "jane.cooper@example.com",
        phone: ""
    },
    {
        name: "Cody",
        lastName: "Fisher",
        position: "Product Directives Officer",
        department: "Intranet",
        urlImage: "assets/img/contacts/cody_fisher.png",
        stateWork: 1,
        pinned: true,
        email: "cody.fisher@example.com",
        phone: ""
    },
    {
        name: "Esther",
        lastName: "Howard",
        position: "Foward Response Manager",
        department: "Directives",
        urlImage: "assets/img/contacts/esther_howard.png",
        stateWork: 1,
        pinned: true,
        email: "esther.howard@example.com",
        phone: ""
    },
    {
        name: "Jenny",
        lastName: "Wilson",
        position: "Central Security Manager",
        department: "Program",
        urlImage: "assets/img/contacts/jenny_wilson.png",
        stateWork: 2,
        pinned: true,
        email: "jenny.wilson@example.com",
        phone: ""
    },
    {
        name: "Kristin",
        lastName: "Watson",
        position: "Internal Applications Manager",
        department: "Seciruty",
        urlImage: "assets/img/contacts/kristin_watson.png",
        stateWork: 1,
        pinned: false,
        email: "kristin.watson@example.com",
        phone: ""
    },
    {
        name: "Cameron",
        lastName: "Williamson",
        position: "Regional Paradigm Technician",
        department: "Optimization",
        urlImage: "assets/img/contacts/cameron_williamson.png",
        stateWork: 0,
        pinned: false,
        email: "cameron.williamson@example.com",
        phone: ""
    },

]