import { Injectable } from '@angular/core';
import { USERS } from 'src/app/constants/users.const';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() { }

  getAllUsers() {
    return USERS;
  }
}
