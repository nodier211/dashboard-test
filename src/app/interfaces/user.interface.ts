export interface IUser {
    name: string;
    lastName: string;
    position: string;
    department: string;
    urlImage: string;
    stateWork: number;
    pinned: boolean;
    email: string;
    phone: string;
}