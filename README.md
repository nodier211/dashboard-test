# DashboardTeam

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.12.

## Fist steps
- npm install
- ng serve (Angular Serve runs in http://localhost:4200/)

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Author
Nodier Momox